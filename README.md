# Mother May I

## Contents of this file

- Introduction
- Recommended modules
- Installation
- Configuration
- Maintainers


## Introduction

If a Drupal website is set to allow users to self-register, it
tend to be plagued by account requests from people and bots that
register to be able to use the site for spamming. The **Mother May I**
module aims at reducing the number of such account requests.

- For a full description of the module, visit the [project page][1].
- To submit bug reports and feature suggestions, or to track changes
  visit the project's [issue tracker][2].
- For community documentation, visit the [documentation page][3].

The major features of **Mother May I** are:

1. The site administrator specifies a secret word.

2. The administrator can also enter a "hint". The hint should be
   descriptive enough that a valid site user (for example, a member of
   the NGO) can easily figure out the secret word from the hint, but
   someone outside the organization cannot. It's up to the
   administrator to decide how cryptic they want the hint to be.

3. If no secret word is defined, this project will not impact the
   account request process.

4. Any account requests with incorrect secret words are logged. You
   can see the results in your site's "Recent log messages".

Note that this project only protects the account registration form. To protect other form targeted by spammers (e.g. the site contact form), use one of the more general CAPTCHA projects available for Drupal.


## Recommended modules

- [Advanced Help][4]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `/admin/help/ah/mothermayi/README.md`.


## Installation

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][5] for further information.

2. Enable the **Mother May I** module on the *Extend* page.  The
   database tables will be created automagically for you at this
   point.

3. Check the *Status report* to see if it necessary to run the
   database update script.

4. Make sure you have the permission "Administer site configuration".

5. Proceed to configure the module, as described in the configuraton
   section below.


## Configuration

On the Mother May I configuration page, you can set the following:

1. The secret word. This needs to be alphanumeric. If no secret word is
   specified, the users requesting accounts won't be asked for one.

2. A tick-box to use a regular expression.

3. A test word. It will be checked against your secret word when the
   configuration is saved. Useful to check that a regular expression
   works as intended.

4. The user hint. If entered, the text will be displayed above the
   secret word entry box on the account register page.

5. Form weight. This lets you move the "secret word" block up and down
  in the account registration form.  The default should usually be
  fine.

If a secret word and optional hint are supplied, users will see a
required "Enter the secret word" box on the account request page.


## Maintainers

**Mother May I** was created by [dwillcox][6] in 2012.  
The current maintainer for D9 is [gisle][7] (Gisle Hannemyr).


[1]: https://www.drupal.org/project/mothermayi
[2]: https://www.drupal.org/project/issues/mothermayi
[3]: https://www.drupal.org/docs/contributed-modules/mother-may-i
[4]: https://www.drupal.org/project/advanced_help
[5]: https://www.drupal.org/docs/extending-drupal/installing-modules
[6]: https://www.drupal.org/u/dwillcox
[7]: https://www.drupal.org/u/gisle
